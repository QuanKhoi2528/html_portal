/*!
 * thuynt1509
 * 
 * 
 * @author thuynt1509
 * @version 2.0.0
 * Copyright 2021. MIT licensed.
 */$(document).ready(function () {
  $("#cal").daterangepicker();
  $("#option-bank a").click(function () {
    $("#option-bank a").removeClass("active");
    $(this).addClass("active");
  });
  $('.toggle').click(function () {
    $(this).toggleClass('active');
    $('.box-left').toggleClass('active');
  });
  $('#slide-top').slick({
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 2.5,
    slidesToScroll: 1,
    autoplay: false,
    arrows: true
  });
  $('#slide-recommend').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    loop: false,
    infinite: false
  });
  $(".lst-option-transport").on('click', 'input:radio', function () {
    $(".lst-option-transport input").prop("checked", false);
    $(".lst-option-transport li").removeClass("active");
    $(this).prop("checked", true);
    $(this).closest('li').toggleClass('active', this.checked);
  });
  $(".lst-transport").on('click', 'input:radio', function () {
    $(".lst-transport input").prop("checked", false);
    $(".lst-transport .item-add-transport").removeClass("active");
    $(this).prop("checked", true);
    $(this).closest('.item-add-transport').toggleClass('active', this.checked);
  });
  $(".box-account-address").on('click', 'input:radio', function () {
    $(".box-account-address input").prop("checked", false);
    $(".box-account-address .item-address").removeClass("active");
    $(this).prop("checked", true);
    $(this).closest('.item-address').toggleClass('active', this.checked);
  });
  $(".lst-date").on('click', 'input:radio', function () {
    $(".lst-date input").prop("checked", false);
    $(".lst-date li").removeClass("active");
    $(this).prop("checked", true);
    $(this).closest('li').toggleClass('active', this.checked);
  });
  $(".box-form-address").on('click', 'input:radio', function () {
    $(".box-form-address input").prop("checked", false);
    $(".box-form-address .item-address").removeClass("active");
    $(this).prop("checked", true);
    $(this).closest('.item-address').toggleClass('active', this.checked);
  }); //
  // $(".card").on('show.bs.collapse', function(){
  //   $(".card").removeClass("active");
  //   $(this).addClass("active");
  // }).on('hide.bs.collapse', function(){
  //   $(".card").removeClass("active");
  // });

  $("#acc-payment .btn-link").click(function () {
    $(".card").removeClass("active");
    $(this).closest('.card').addClass('active', this.checked);
  });
});
$(".collapse").map((index, item) => {
  if ($(item).hasClass("show") == true) {
    $($(item).parent()).addClass("active");
  }
}); //custom select option PCS start

var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/

x = document.getElementsByClassName("custom-select");
l = x.length;

for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/

  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/

  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");

  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function (e) {
      /*when an item is clicked, update the original select box,
      and the selected item:*/
      var y, i, k, s, h, sl, yl;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      sl = s.length;
      h = this.parentNode.previousSibling;

      for (i = 0; i < sl; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          yl = y.length;

          for (k = 0; k < yl; k++) {
            y[k].removeAttribute("class");
          }

          this.setAttribute("class", "same-as-selected");
          break;
        }
      }

      h.click();
    });
    b.appendChild(c);
  }

  x[i].appendChild(b);
  a.addEventListener("click", function (e) {
    /*when the select box is clicked, close any other select boxes,
    and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}

function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x,
      y,
      i,
      xl,
      yl,
      arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;

  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i);
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }

  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/


document.addEventListener("click", closeAllSelect); //custom select option PCS end