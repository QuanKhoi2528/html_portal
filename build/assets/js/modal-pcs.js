/*!
 * thuynt1509
 * 
 * 
 * @author thuynt1509
 * @version 2.0.0
 * Copyright 2021. MIT licensed.
 */$(document).ready(function () {
  $('.toggle').click(function () {
    $(this).toggleClass('active');
    $('.box-left').toggleClass('active');
  }); // $('.slider-for').slick({
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  //   arrows: false,
  //   fade: true,
  //   asNavFor: '.slider-nav',
  //   loop: false,
  // });
  // $('.slider-nav').slick({
  //   slidesToShow: 5,
  //   slidesToScroll: 1,
  //   asNavFor: '.slider-for',
  //   dots: false,
  //   focusOnSelect: true,
  //   loop: false,
  //   responsive: [
  //     {
  //       breakpoint: 1200,
  //       settings: {
  //         slidesToShow: 4,
  //       }
  //     },
  //
  //   ]
  // });

  $('.modal-pcs').on('shown.bs.modal', function () {
    $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.slider-nav',
      loop: false
    });
    $('.slider-nav').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '.slider-for',
      dots: false,
      focusOnSelect: true,
      loop: false,
      responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 4
        }
      }]
    });
  });
});